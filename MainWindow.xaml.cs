﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Web.Script.Serialization;
using Microsoft.Win32;
using System.Windows.Media;

namespace Parcer3D
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int timer; //Таймер
        System.Windows.Threading.DispatcherTimer dispatcherTimer; // Ссылка на таймер
        System.Windows.Forms.NotifyIcon notifyIcon; //Ссылка на иконку с трее
        System.Windows.Forms.ContextMenu contextMenu;
        System.Windows.Forms.MenuItem menuItem;
        System.ComponentModel.IContainer components;
        List<Profile> profiles; //Лист с профилями
        Profile profile; //Ссылка на динамический профиль
        bool isAutoSave;
        public MainWindow()
        {
            InitializeComponent();
            Load();
        }

        //Загружаем последнюю позицию и размер окна, новый экхемпляр листа с профилями и таймер
        private void Load()
        {
            this.Top = Properties.Settings.Default.WindowTop;
            this.Left = Properties.Settings.Default.WindowLeft;
            this.Height = Properties.Settings.Default.WindowHeight;
            this.Width = Properties.Settings.Default.WindowWidth;
            var name = Properties.Settings.Default.CurrentProfile;
            isAutoSave = Properties.Settings.Default.isAutoSave;
            autoSave.IsChecked = isAutoSave;
            TrayIcon();
            this.ShowInTaskbar = false;
            dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Interval = new TimeSpan(0, 0, 1);
            //Таймер, который автоматом проферяет на наличие файла out.exe и открывает его загружая в тексбокс 2-ой вкладки
            dispatcherTimer.Tick += TimerDojob;

            JSONRead();
            CurrentProfile(name);
            CreateOrReWriteTxt();
        }

        #region Отображение окна, его позиция, размер, иконка в трее, ее поведение и т.д.
        private void Save()
        {
            Properties.Settings.Default.WindowTop = this.Top;
            Properties.Settings.Default.WindowLeft = this.Left;
            Properties.Settings.Default.WindowHeight = this.Height;
            Properties.Settings.Default.WindowWidth = this.Width;
            Properties.Settings.Default.CurrentProfile = profile.Name;
            Properties.Settings.Default.isAutoSave = isAutoSave;
            Properties.Settings.Default.Save();
        }

        private void TrayIcon()
        {
            components = new System.ComponentModel.Container();
            contextMenu = new System.Windows.Forms.ContextMenu();
            menuItem = new System.Windows.Forms.MenuItem();
            notifyIcon = new System.Windows.Forms.NotifyIcon(components);
            contextMenu.MenuItems.AddRange(
                    new System.Windows.Forms.MenuItem[] { menuItem });
            menuItem.Index = 0;
            menuItem.Text = "E&xit";
            menuItem.Click += (sender, args) => this.Close();
            notifyIcon.ContextMenu = contextMenu;
            notifyIcon.Icon = new System.Drawing.Icon("Main.ico");
            notifyIcon.Visible = true;
            notifyIcon.Click += (sender, args) =>
           {
               this.Visibility = Visibility.Visible;
               this.WindowState = WindowState.Normal;
               this.Activate();
           };
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Save();
            JSONWrite();
            DeleteOutTxt();
            components.Dispose();
        }
        #endregion

        #region JSON

        //работа с JSON
        private void JSONWrite()
        {
            JavaScriptSerializer JSON = new JavaScriptSerializer();

            var profilesJSON = JSON.Serialize(profiles);
            if (!File.Exists("profiles"))
            {
                using (StreamWriter sw = File.CreateText("profiles"))
                {
                    sw.WriteLine(profilesJSON);
                }
            }
            else
                File.WriteAllText("profiles", profilesJSON);
        }

        //Вычитываем сохранившиеся профили из текущей папки, если файла нет, просто создаем новые
        private void JSONRead()
        {
            string tmp;

            if (File.Exists("profiles"))
            {
                tmp = File.ReadAllText("profiles");
                JavaScriptSerializer JSON = new JavaScriptSerializer();
                profiles = JSON.Deserialize<List<Profile>>(tmp);
            }
            else
            {
                profiles = new List<Profile>();
                for (int i = 12; i < 27; i++)
                {
                    profiles.Add(new Profile() { Name = (i).ToString() });
                }
            }
        }

        #endregion

        private void DeleteOutTxt()
        {
            if (File.Exists("out.txt"))
                File.Delete("out.txt");
            if (File.Exists("in.txt"))
                File.WriteAllText("in.txt", String.Empty);
            if (File.Exists("ink.txt"))
                File.WriteAllText("ink.txt", String.Empty);
            if (File.Exists("scr.txt"))
                File.WriteAllText("scr.txt", String.Empty);
            if (File.Exists("vid.txt"))
                File.WriteAllText("vid.txt", String.Empty);
        }

        //Создаем временные файлы in, ink, scr, vid в корне папки и записываем туда инфу с ТестБлоков или если созданы, просто записываем 
        private void CreateOrReWriteTxt()
        {
            string tmp = null;
            ClearRichTextBox();
            using (var tw = new StreamWriter("in.txt", false))
            {
                if (File.Exists(profile.Intxt))
                {
                    tmp = File.ReadAllText(profile.Intxt).Trim();
                    tw.WriteLine(tmp);
                    richTextBox1.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                    inCheck.IsChecked = true;
                }
                else
                    inCheck.IsChecked = false;
            }
            using (var tw = new StreamWriter("ink.txt", false))
            {
                if (File.Exists(profile.Inktxt))
                {
                    tmp = File.ReadAllText(profile.Inktxt).Trim();
                    tw.WriteLine(tmp);
                    richTextBox2.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                    inkCheck.IsChecked = true;
                }
                else
                    inkCheck.IsChecked = false;
            }
            using (var tw = new StreamWriter("scr.txt", false))
            {
                if (File.Exists(profile.Scrtxt))
                {
                    tmp = File.ReadAllText(profile.Scrtxt).Trim();
                    tw.WriteLine(tmp);
                    richTextBox3.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                    scrCheck.IsChecked = true;
                }
                else
                    scrCheck.IsChecked = false;
            }
            using (var tw = new StreamWriter("vid.txt", false))
            {
                if (File.Exists(profile.Vidtxt))
                {
                    tmp = File.ReadAllText(profile.Vidtxt).Trim();
                    tw.WriteLine(tmp);
                    richTextBox4.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                    vidCheck.IsChecked = true;
                }
                else
                    vidCheck.IsChecked = false;
            }
            out1Check.IsChecked = File.Exists(profile.Outtxt1) ? true : false;
            outCCheck.IsChecked = File.Exists(profile.OuttxtC) ? true : false;
            TextBoxTextInfo();
        }

        private void UncheckAllButtons()
        {
            inCheck.IsChecked = false;
            inkCheck.IsChecked = false;
            scrCheck.IsChecked = false;
            vidCheck.IsChecked = false;
            out1Check.IsChecked = false;
            outCCheck.IsChecked = false;
        }

        private void ClearAllInfoBlocks()
        {
            out1Info.Content = string.Empty;
            outCInfo.Content = string.Empty;
            inInfo.Text = string.Empty;
            inkInfo.Text = string.Empty;
            scrInfo.Text = string.Empty;
            vidInfo.Text = string.Empty;
        }

        private void ClearRichTextBox()
        {
            richTextBox1.Document.Blocks.Clear();
            richTextBox2.Document.Blocks.Clear();
            richTextBox3.Document.Blocks.Clear();
            richTextBox4.Document.Blocks.Clear();
            richTextBox5.Document.Blocks.Clear();
        }
        private void TextBoxTextInfo()
        {
            inInfo.Text = File.Exists(profile.Intxt) ? System.IO.Path.GetFileName(profile.Intxt) : "";
            inkInfo.Text = File.Exists(profile.Inktxt) ? System.IO.Path.GetFileName(profile.Inktxt) : "";
            scrInfo.Text = File.Exists(profile.Scrtxt) ? System.IO.Path.GetFileName(profile.Scrtxt) : "";
            vidInfo.Text = File.Exists(profile.Vidtxt) ? System.IO.Path.GetFileName(profile.Vidtxt) : "";
            out1Info.Content = File.Exists(profile.Outtxt1) ? System.IO.Path.GetFileName(profile.Outtxt1) : "";
            outCInfo.Content = File.Exists(profile.OuttxtC) ? System.IO.Path.GetFileName(profile.OuttxtC) : "";
        }


        //Метод записывающий весь текст из текстбокаса в свои txt файлы 
        private void WriteAllFiles()
        {
            if (File.Exists("in.txt"))
                File.WriteAllText("in.txt", new TextRange(richTextBox1.Document.ContentStart, richTextBox1.Document.ContentEnd).Text);
            if (File.Exists("ink.txt"))
                File.WriteAllText("ink.txt", new TextRange(richTextBox2.Document.ContentStart, richTextBox2.Document.ContentEnd).Text);
            if (File.Exists("scr.txt"))
                File.WriteAllText("scr.txt", new TextRange(richTextBox3.Document.ContentStart, richTextBox3.Document.ContentEnd).Text);
            if (File.Exists("vid.txt"))
                File.WriteAllText("vid.txt", new TextRange(richTextBox4.Document.ContentStart, richTextBox4.Document.ContentEnd).Text);
        }

        //Берем ссылку на текущий профиль
        private void CurrentProfile(string name)
        {
            if (String.IsNullOrEmpty(name))
            {
                profile = profiles.First();
                (WrapPaneRadioButtons.Children[0] as RadioButton).IsChecked = true;
            }
            else
            {
                profile = profiles.Find(x => x.Name == name);
                foreach (var item in WrapPaneRadioButtons.Children)
                {
                    if ((item as RadioButton).Content as string == profile.Name)
                    {
                        (item as RadioButton).Checked -= RadioButton_Click;
                        (item as RadioButton).IsChecked = true;
                        (item as RadioButton).Checked += RadioButton_Click;
                        break;
                    }
                }
            }
        }

        private void TimerDojob(object sender, EventArgs e)
        {
            timer--;
            if (File.Exists("out.txt"))
            {
                string tmp;
                dispatcherTimer.Stop();
                outTab.IsSelected = true;
                richTextBox5.Document.Blocks.Clear();
                if (File.Exists("out.txt"))
                {
                    tmp = File.ReadAllText("out.txt");
                    richTextBox5.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                    if (isAutoSave)
                    {
                        SaveOut(tmp);
                    }
                }
            }
            if (timer <= 0)
            {
                dispatcherTimer.Stop();
                MessageBox.Show("Прошло 30 сек, а файла out.txt нет");
            }
        }


        //Обработчик на кнопку профилей
        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            var name = (sender as RadioButton).Content as string;
            profile = profiles.Find(x => x.Name == name);
            CreateOrReWriteTxt();
        }

        //Оброботчики для кнопок путей к файлам
        private void ButtonPath_Click(object sender, RoutedEventArgs e)
        {
            string tmp;
            string name = (sender as CheckBox).Content as string;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Text|*.txt|All|*.*";

            switch (name)
            {
                case "in.txt":
                    openFileDialog.InitialDirectory = File.Exists(profile.Intxt) ? profile.Intxt : Environment.CurrentDirectory;
                    if (openFileDialog.ShowDialog() == true)
                    {
                        profile.Intxt = openFileDialog.FileName;
                        using (var tw = new StreamWriter("in.txt", false))
                        {
                            tmp = File.ReadAllText(profile.Intxt).Trim();
                            tw.WriteLine(tmp);
                            richTextBox1.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                            inCheck.IsChecked = true;
                        }
                    }
                    else if (!File.Exists(profile.Intxt))
                        inCheck.IsChecked = false;
                    else
                        inCheck.IsChecked = true;
                    break;
                case "ink.txt":
                    openFileDialog.InitialDirectory = File.Exists(profile.Inktxt) ? profile.Inktxt : Environment.CurrentDirectory;
                    if (openFileDialog.ShowDialog() == true)
                    {
                        profile.Inktxt = openFileDialog.FileName;
                        using (var tw = new StreamWriter("ink.txt", false))
                        {
                            tmp = File.ReadAllText(profile.Inktxt).Trim();
                            tw.WriteLine(tmp);
                            richTextBox2.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                            inkCheck.IsChecked = true;
                        }
                    }
                    else if (!File.Exists(profile.Inktxt))
                        inkCheck.IsChecked = false;
                    else
                        inkCheck.IsChecked = true;
                    break;
                case "scr.txt":
                    openFileDialog.InitialDirectory = File.Exists(profile.Scrtxt) ? profile.Scrtxt : Environment.CurrentDirectory;
                    if (openFileDialog.ShowDialog() == true)
                    {
                        profile.Scrtxt = openFileDialog.FileName;
                        using (var tw = new StreamWriter("scr.txt", false))
                        {
                            tmp = File.ReadAllText(profile.Scrtxt).Trim();
                            tw.WriteLine(tmp);
                            richTextBox3.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                            scrCheck.IsChecked = true;
                        }
                    }
                    else if (!File.Exists(profile.Scrtxt))
                        scrCheck.IsChecked = false;
                    else
                        scrCheck.IsChecked = true;
                    break;
                case "vid.txt":
                    openFileDialog.InitialDirectory = File.Exists(profile.Vidtxt) ? profile.Vidtxt : Environment.CurrentDirectory;
                    if (openFileDialog.ShowDialog() == true)
                    {
                        profile.Vidtxt = openFileDialog.FileName;
                        using (var tw = new StreamWriter("vid.txt", false))
                        {
                            tmp = File.ReadAllText(profile.Vidtxt).Trim();
                            tw.WriteLine(tmp);
                            richTextBox4.Document.Blocks.Add(new Paragraph(new Run(tmp)));
                            vidCheck.IsChecked = true;
                        }
                    }
                    else if (!File.Exists(profile.Vidtxt))
                        vidCheck.IsChecked = false;
                    else
                        vidCheck.IsChecked = true;
                    break;
                case "out_1.txt":
                    openFileDialog.InitialDirectory = File.Exists(profile.Outtxt1) ? profile.Outtxt1 : Environment.CurrentDirectory;
                    if (openFileDialog.ShowDialog() == true)
                    {
                        profile.Outtxt1 = openFileDialog.FileName;
                        richTextBox5.Document.Blocks.Clear();
                        out1Check.IsChecked = true;
                    }
                    else if (!File.Exists(profile.Outtxt1))
                        out1Check.IsChecked = false;
                    else
                        out1Check.IsChecked = true;
                    break;
                case "out_c.txt":
                    openFileDialog.InitialDirectory = File.Exists(profile.OuttxtC) ? profile.OuttxtC : Environment.CurrentDirectory;
                    if (openFileDialog.ShowDialog() == true)
                    {
                        profile.OuttxtC = openFileDialog.FileName;
                        richTextBox5.Document.Blocks.Clear();
                        outCCheck.IsChecked = true;
                    }
                    else if (!File.Exists(profile.OuttxtC))
                        outCCheck.IsChecked = false;
                    else
                        outCCheck.IsChecked = true;
                    break;
            }
            TextBoxTextInfo();
        }

        //Обработчик кнопки копировать весь текст из тесктбокса Out
        private void ButtonCopy_Click(object sender, RoutedEventArgs e)
        {
            richTextBox5.SelectAll();
            richTextBox5.Copy();
        }

        //Обработчик кнопки старт, который запускает програмку лежащую в той же папки и парсит файлы
        private void ButtonStart_Click(object sender, RoutedEventArgs e)
        {
            timer = 60; //60 сек для автоматического отключения таймера
            DeleteOutTxt();
            WriteAllFiles();
            if (File.Exists("parse.bat"))
            {
                Process.Start("parse.bat");
                dispatcherTimer.Start();
            }
        }

        private void SaveOut(string tmp)
        {
            if (File.Exists(profile.Outtxt1))
            {
                using (var tw = new StreamWriter(profile.Outtxt1, false))
                {
                    tw.WriteLine(tmp);
                    out1Info.Foreground = Brushes.Green;
                }
            }
            else
                MessageBox.Show("Не указан путь для out_1.txt");
            if (File.Exists(profile.OuttxtC))
            {
                using (var tw = new StreamWriter(profile.OuttxtC, false))
                {
                    tw.WriteLine(tmp);
                    outCInfo.Foreground = Brushes.Green;
                }
            }
            else
                MessageBox.Show("Не указан путь для out_c.txt");
        }

        //Обработчик кнопки сохранить Out файл
        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            SaveOut(new TextRange(richTextBox5.Document.ContentStart, richTextBox5.Document.ContentEnd).Text);
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            CreateOrReWriteTxt();
        }

        private void ButtonClear_Click(object sender, RoutedEventArgs e)
        {
            //  ClearAllInfoBlocks();
            ClearRichTextBox();
            DeleteOutTxt();
            //  UncheckAllButtons();
            // profile.Intxt = profile.Inktxt = profile.Scrtxt = profile.Vidtxt = profile.Outtxt1 = profile.OuttxtC = String.Empty;
        }

        private void CheckBoxIsAutoSave_Checked(object sender, RoutedEventArgs e)
        {
            isAutoSave = true;
        }

        private void CheckBoxIsAutoSave_Unchecked(object sender, RoutedEventArgs e)
        {
            isAutoSave = false;
        }

        private void ButtonCloseTextBlock_Click(object sender, RoutedEventArgs e)
        {
            switch ((sender as Button).Name)
            {
                case "inClose":
                    //  inCheck.IsChecked = false;
                    //  inInfo.Text = String.Empty;
                    // profile.Intxt = String.Empty;
                    richTextBox1.Document.Blocks.Clear();
                    break;
                case "inkClose":
                    //  inkCheck.IsChecked = false;
                    //  inkInfo.Text = String.Empty;
                    //  profile.Inktxt = String.Empty;
                    richTextBox2.Document.Blocks.Clear();
                    break;
                case "scrClose":
                    //  scrCheck.IsChecked = false;
                    //  scrInfo.Text = String.Empty;
                    // profile.Scrtxt = String.Empty;
                    richTextBox3.Document.Blocks.Clear();
                    break;
                case "vidClose":
                    //  vidCheck.IsChecked = false;
                    //  vidInfo.Text = String.Empty;
                    //  profile.Vidtxt = String.Empty;
                    richTextBox4.Document.Blocks.Clear();
                    break;
            }
        }

        private void RichTextBox5_TextChanged(object sender, TextChangedEventArgs e)
        {
            out1Info.Foreground = Brushes.Black;
            outCInfo.Foreground = Brushes.Black;
        }

        private void ButtonEndRichBox_Click(object sender, RoutedEventArgs e)
        {
            richTextBox5.ScrollToEnd();
        }

        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((sender as Button).Tag as string == "out")
                    Process.Start(Path.GetDirectoryName(profile.Outtxt1));
            }
            catch { }
            try
            {
                if ((sender as Button).Tag as string == "out")
                    Process.Start(Path.GetDirectoryName(profile.OuttxtC));
            }
            catch { }
            try
            {
                if ((sender as Button).Tag as string == "in")
                    Process.Start(Path.GetDirectoryName(profile.Intxt));
            }
            catch{}
            try
            {
                if ((sender as Button).Tag as string == "in")
                    Process.Start(Path.GetDirectoryName(profile.Inktxt));
            }
            catch{}
            try
            {
                if ((sender as Button).Tag as string == "in")
                    Process.Start(Path.GetDirectoryName(profile.Vidtxt));
            }
            catch{}
            try
            {
                if ((sender as Button).Tag as string == "in")
                    Process.Start(Path.GetDirectoryName(profile.Scrtxt));
            }
            catch{}
        }
    }
}
